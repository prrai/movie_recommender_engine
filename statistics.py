import pandas as pd
import math
import pickle
import sys
import math
# from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from math import sqrt
from movieMapper import displayMovieInfo, displayUserWatchHistory
# from sklearn.model_selection import train_test_split

def rmse(prediction, ground_truth):
    # prediction = prediction[ground_truth.nonzero()].flatten()
    # ground_truth = ground_truth[ground_truth.nonzero()].flatten()
    print(prediction,ground_truth)
    return sqrt(mean_squared_error(prediction, ground_truth))

def get_k_most_popular_movies(movies_similarity_matrix):
    movies_similarity_matrix['average'] = movies_similarity_matrix.mean(numeric_only=True, axis=1)
    movie_list=movies_similarity_matrix[['average']]
    movie_list= movie_list.sort('average', ascending=False)
    return movie_list

def get_data_ready(ratings_dataframe):
    ratings_dataframe = ratings_dataframe.pivot(index='userId', columns='movieId', values='rating')
    movies_similarity_dataframe = ratings_dataframe.corr(method='pearson', min_periods=1)
    with open('movies_similarity_dataframe.pickle', 'wb') as f:
        pickle.dump(movies_similarity_dataframe, f)
    return ratings_dataframe

def accuracy_calculation():
    k=10
    ratings_file_path = 'ml-100k/u.data'
    # get_data_ready(ratings_file_path)
    header = ['userId', 'movieId', 'rating', 'timestamp']
    ratings_dataframe = pd.read_csv('ml-100k/u.data', sep='\t', names=header)
    train_data, test_data = train_test_split(ratings_dataframe, test_size=0.20)
    actual = []
    pred = []
    print(train_data.shape, test_data.shape)

    train_data.fillna(0,inplace = True)
    train_data = train_data.pivot(index='userId', columns='movieId', values='rating')
    test_data = test_data.pivot(index='userId', columns='movieId', values='rating')
    print(train_data.head(5))
    print(test_data.head(5))
    print(train_data.shape,test_data.shape)
    movies_similarity_matrix = train_data.corr(method='spearman', min_periods=1)
    n_users = list(test_data.index)
    print(n_users)
    for user in n_users:
        ratings_prediction_test = test_data.loc[[user]].copy()
        ratings_prediction_train = train_data.loc[[user]].copy()
        movies_rated_train = []

        for col in ratings_prediction_train.columns:
            if (not math.isnan(ratings_prediction_train.ix[user, col])):
                movies_rated_train.append(col)

        movies_rated_test = []
        for col in ratings_prediction_test.columns:
            if (not math.isnan(ratings_prediction_test.ix[user, col])):
                movies_rated_test.append(col)

        check_prediction_dict = {}

        for movie in movies_rated_test:
            if(not movie in list(movies_similarity_matrix.columns)):
                continue
            similarity_list = movies_similarity_matrix.ix[movie, movies_rated_train]
            similarity_list.sort_values(inplace=True, ascending=False)
            similarity_list = similarity_list[0:k]
            num = 0
            deno = 0
            for similar_movie in list(similarity_list.index):
                num += similarity_list[similar_movie] * train_data.ix[user, similar_movie]
                deno += math.fabs(similarity_list[similar_movie])
            prediction = float(num / deno)
            check_prediction_dict[movie] = prediction

        print(user,check_prediction_dict)
        for key, value in check_prediction_dict.items():
            if not math.isnan(value):
                value = math.ceil(value * 100)/100
                pred.append(value)
                actual.append(ratings_prediction_test.ix[user, key])

    rmse1 = rmse(pred,actual)
    print("The root mean square is " , rmse1)


def main():
    ratings_file_path = 'ml-100k/u.data'
    header = ['userId', 'movieId', 'rating', 'timestamp']
    ratings_dataframe = pd.read_csv(ratings_file_path, sep='\t', names=header)

    # print(ratings_dataframe.ix[:,2])

    rfd = ratings_dataframe.ix[:,2]
    # print rfd
    num_of_1 = 0
    num_of_2 = 0
    num_of_3 = 0
    num_of_4 = 0
    num_of_5 = 0

    for item in rfd:
        if not math.isnan(item):
            if int(item) == 1:
                num_of_1+=1
            elif int(item) == 2:
                num_of_2+=1
            elif int(item) == 3:
                num_of_3+= 1
            elif int(item) == 4:
                num_of_4+= 1
            elif int(item) == 5:
                num_of_5+= 1

    print(num_of_1,num_of_2,num_of_3,num_of_4,num_of_5)

    ratings_dataframe=get_data_ready(ratings_dataframe)
    with open('movies_similarity_dataframe.pickle','rb') as f:
        movies_similarity_matrix = pickle.load(f)
    user = int(sys.argv[1])
    k=int(sys.argv[2])

    
    ratings_prediction=ratings_dataframe.loc[[user]].copy()
    movies_rated=[]
    movies_not_rated=[]
    get_k_most_popular_movies(movies_similarity_matrix)

    for col in ratings_prediction.columns:
        if(not math.isnan(ratings_prediction.ix[user,col])):
            movies_rated.append(col)
        else:
            movies_not_rated.append(col)
    if(movies_rated.__len__()<k):
        ordered_recommendation=get_k_most_popular_movies(movies_similarity_matrix)
    else:
        check_prediction_dict = {}
        prediction_dict={}

        for movie in movies_not_rated:
            similarity_list=movies_similarity_matrix.ix[movie,movies_rated]
            similarity_list.sort_values(inplace=True,ascending=False)
            similarity_list=similarity_list[0:k]
            num = 0
            deno = 0
            for similar_movie in list(similarity_list.index):
                num+=similarity_list[similar_movie]*ratings_dataframe.ix[user,similar_movie]
                deno+=math.fabs(similarity_list[similar_movie])
            prediction=float(num/deno)
            prediction_dict[movie]=prediction
        ordered_recommendation = (sorted(prediction_dict,key=lambda k: (prediction_dict[k], -k), reverse=True))


        check_prediction_dict = {}

        for movie in movies_rated:
                similarity_list=movies_similarity_matrix.ix[movie,movies_rated]
                similarity_list.sort_values(inplace=True,ascending=False)
                similarity_list=similarity_list[0:k]
                num = 0
                deno = 0
                for similar_movie in list(similarity_list.index):
                    num+=similarity_list[similar_movie]*ratings_dataframe.ix[user,similar_movie]
                    deno+=math.fabs(similarity_list[similar_movie])
                prediction=float(num/deno)
                check_prediction_dict[movie]=prediction

    print("Movies rated by the user with Id {0} are: ".format(user))
    print(movies_rated)
    displayUserWatchHistory(movies_rated)
    print("Top 3 recommended movies for userId ",user," are : ",ordered_recommendation[0:3])
    displayMovieInfo(ordered_recommendation[0:3])

if __name__ == '__main__':
    main()
    # accuracy_calculation()
