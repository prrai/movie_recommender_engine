import linecache


def _get_genre_dictionary():
    genre_dict = {}
    with open('ml-100k/u.genre', mode='r') as gen:
        data = gen.read()
        for line in data.split('\n'):
            if len(line) > 0:
                value, key = line.split('|')
                genre_dict[int(key)] = value
    return genre_dict


def displayUserWatchHistory(movie_list):
    genre_dict = _get_genre_dictionary()
    genres = {}
    for key, value in genre_dict.items():
        genres[value] = 0
    print('################################################################################')
    for movie in movie_list:
        movie_info = linecache.getline('ml-100k/u.item', int(movie))
        _, title, release_date, _, imdb_url, genre_array = movie_info.split('|')[:6]
        genre_array = movie_info.split('|')[6:]
        for index in range(len(genre_array)):
            if '\n' in genre_array[index]:
                genre_array[index].strip('\n')
            if genre_array[index] == '1':
                genres[genre_dict[index]] += 1
        print(title)
    print('\nGenre Frequency\n')
    for key, value in genres.items():
        print('{0} => {1}'.format(key, value))
    print('################################################################################\n\n')


def displayMovieInfo(movie_list):
    genre_dict = _get_genre_dictionary()
    for movie in movie_list:
        print('################################################################################')
        movie_info = linecache.getline('ml-100k/u.item', int(movie))
        _, title, release_date, _, imdb_url, genre_array = movie_info.split('|')[:6]
        genre_array = movie_info.split('|')[6:]
        genres = []
        for index in range(len(genre_array)):
            if '\n' in genre_array[index]:
                genre_array[index].strip('\n')
            if genre_array[index] == '1':
                genres.append(genre_dict[index])
        print('Title: {0}'.format(title))
        print('Release date: {0}'.format(release_date))
        print('IMDB URL: {0}'.format(imdb_url))
        print('Genres: {0}'.format(genres))
        print('################################################################################\n\n')

if __name__ == '__main__':

    all_movie_list = []

    for i in range(1,1683):
        all_movie_list.append(i)

    print(all_movie_list)

    displayUserWatchHistory(all_movie_list)