import sys
 
#To show some messages:
import recsys.algorithm
#recsys.algorithm.VERBOSE = True
 
from recsys.algorithm.factorize import SVD
from recsys.datamodel.data import Data
from recsys.evaluation.prediction import RMSE, MAE
from recsys.evaluation.decision import PrecisionRecallF1
from recsys.evaluation.ranking import SpearmanRho, KendallTau
from math import sqrt
from movieMapper import displayMovieInfo, displayUserWatchHistory




import numpy as np
import pandas as pd

from sklearn import cross_validation as cv
from sklearn.metrics.pairwise import pairwise_distances

import scipy.sparse as sp
from scipy.sparse.linalg import svds


def main(h):
    #Dataset
    PERCENT_TRAIN = 80
    data = Data()
    data.load('ml-100k/u.data', sep='\t', format={'col':0, 'row':1, 'value':2, 'ids':int})


    #Train & Test data
    train, test = data.split_train_test(percent=PERCENT_TRAIN)

    #Create SVD
    K=100
    svd = SVD()
    svd.set_data(train)

    svd.compute(k=K, min_values=1, pre_normalize=None, mean_center=True, post_normalize=True)
    #svd.compute(k=K, min_values=5, pre_normalize=None, mean_center=True, post_normalize=True)
    #svd.compute(k=K, pre_normalize=None, mean_center=True, post_normalize=True)

    print('')
    print('COMPUTING SIMILARITY')
    print(svd.similarity(h, 2)) # similarity between items
    print(svd.similar(h, 5)) # show 5 similar items

    print('')
    print('GENERATING PREDICTION')
    MIN_RATING = 0.0
    MAX_RATING = 5.0
    ITEMID = 1
    USERID = h
    print(svd.predict(ITEMID, USERID, MIN_RATING, MAX_RATING)) # predicted rating value
    print(svd.get_matrix().value(ITEMID, USERID)) # real rating value

    print('')
    print('GENERATING RECOMMENDATION')
    recommended_movies = svd.recommend(USERID, n=5, only_unknowns=True, is_row=False)
    # print recommended_movies
    
    movies_recommended = [] 

    for item in recommended_movies:
        movies_recommended.append(item[0])

    print("Movies rated by the user with Id {0} are: ".format(h))
    # displayUserWatchHistory(movies_rated)
    print("Top 3 recommended movies for userId ",h," are : ",movies_recommended[0:5])
    displayMovieInfo(movies_recommended[0:5])

    
    # print(svd.recommend(USERID, n=5, only_unknowns=True, is_row=False))

    #Evaluation using prediction-based metrics
    rmse = RMSE()
    mae = MAE()
    spearman = SpearmanRho()
    kendall = KendallTau()
    #decision = PrecisionRecallF1()
    for rating, item_id, user_id in test.get():
        try:
            pred_rating = svd.predict(item_id, user_id)
            rmse.add(rating, pred_rating)
            mae.add(rating, pred_rating)
            spearman.add(rating, pred_rating)
            kendall.add(rating, pred_rating)
        except KeyError:
            continue

    print('')
    print('EVALUATION RESULT')
    print('RMSE=%s' % rmse.compute())
    print('MAE=%s' % mae.compute())
    print('Spearman\'s rho=%s' % spearman.compute())
    print('Kendall-tau=%s' % kendall.compute())
    print('')


def svd1(user):
    header = ['userId', 'movieId', 'rating', 'timestamp']
    df = pd.read_csv('datasets/u.data', sep=' ', names=header)
    n_users = df.userId.shape[0]
    n_movies = df.movieId.shape[0]
    print('Number of users = ' + str(n_users) + ' | Number of movies = ' + str(n_movies))
    train_data, test_data = cv.train_test_split(df, test_size=0.25)

    print(type(train_data))
    print(type(test_data))

    # train_data = pd.DataFrame(train_data)
    # test_data = pd.DataFrame(test_data)

    # print(train_data.shape)
    # print(test_data.shape)

    # max_movie_num = -1
    # max_user_num = -1
    # for in_line in train_data:
    #     curr_user_num = in_line[0]
    #     if max_user_num < curr_user_num:
    #         max_user_num = curr_user_num
    # print(max_user_num)
    #
    # n_users = int(max_user_num)
    #
    # for in_line in train_data:
    #     curr_movie_num = in_line[1]
    #     if max_movie_num < curr_movie_num:
    #         max_movie_num = curr_movie_num
    # print(max_movie_num)

    # n_movies = int(max_movie_num)

    ratings_dataframe = pd.read_csv('datasets/ratings.csv')
    rp = ratings_dataframe.pivot_table(index='movieId', columns='userId', values='rating')

    rating_user_id_dataframe = rp[user]
    similar_user_id = rp.corrwith(rating_user_id_dataframe)
    print(similar_user_id)

    rating_c = ratings_dataframe[(rating_user_id_dataframe[ratings_dataframe.movieId].isnull().values) & (ratings_dataframe.userId != user)]

    rating_c['similarity'] =  rating_c['userId'].map(similar_user_id.get)

    rating_c['sim_rating'] = rating_c.similarity * rating_c.rating
    recommendation = rating_c.groupby('movieId').apply(lambda s: s.sim_rating.sum() / s.similarity.sum())
    print "The top 5 recommendations based on User-based CF \n" , recommendation.sort_values(ascending=False)[0:5]



    # item_similarity = pairwise_distances(rp.T, metric='cosine')


    #get SVD components from train matrix. Choose k.
    u, s, vt = svds(rp, k = 20)
    print u
    print s
    print vt
    s_diag_matrix=np.diag(s)
    X_pred = np.dot(np.dot(u, s_diag_matrix), vt)
    print(X_pred)


if __name__ == '__main__':
    h = int(sys.argv[1])
    main(h)
    # user = int(sys.argv[1])
    # svd1(user)
