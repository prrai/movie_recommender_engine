import pandas as pd
import pickle
import sys
import math
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from math import sqrt
from movieMapper import displayMovieInfo, displayUserWatchHistory

def rmse(prediction, ground_truth):
    return sqrt(mean_squared_error(prediction, ground_truth))

def get_k_most_popular_movies():
    with open('popular_movies.pickle', 'rb') as f:
        movie_list = pickle.load(f)
    print(movie_list)
    return movie_list

def get_data_ready(file_path):
    header = ['userId', 'movieId', 'rating', 'timestamp']
    ratings_dataframe_tmp = pd.read_csv(file_path, sep='\t', names=header)
    ratings_dataframe = ratings_dataframe_tmp.pivot(index='movieId', columns='userId', values='rating')
    users_similarity_dataframe = ratings_dataframe.corr(method='pearson', min_periods=1)
    ratings_dataframe = ratings_dataframe_tmp.pivot(index='userId', columns='movieId', values='rating')
    with open('users_similarity_dataframe.pickle', 'wb') as f:
        pickle.dump(users_similarity_dataframe, f)
    with open('ratings_dataframe.pickle', 'wb') as f:
        pickle.dump(ratings_dataframe, f)

def accuracy_calculation():
    k=50
    ratings_file_path = 'ml-100k/u.data'
    header = ['userId', 'movieId', 'rating', 'timestamp']
    ratings_dataframe = pd.read_csv(ratings_file_path, sep='\t', names=header)
    train_data_tmp, test_data = train_test_split(ratings_dataframe, test_size=0.20)
    actual = []
    pred = []
    print(train_data_tmp.shape, test_data.shape)

    train_data_tmp.fillna(0,inplace = True)
    train_data = train_data_tmp.pivot(index='movieId', columns='userId', values='rating')
    test_data = test_data.pivot(index='userId', columns='movieId', values='rating')
    users_similarity_matrix = train_data.corr(method='pearson', min_periods=1)
    train_data = train_data_tmp.pivot(index='userId', columns='movieId', values='rating')
    n_users = list(test_data.index)
    print(n_users)
    for user in n_users:
        ratings_prediction_test = test_data.loc[[user]].copy()
        ratings_prediction_train = train_data.loc[[user]].copy()
        movies_rated_train = []

        for col in ratings_prediction_train.columns:
            if (not math.isnan(ratings_prediction_train.ix[user, col])):
                movies_rated_train.append(col)

        movies_rated_test = []
        for col in ratings_prediction_test.columns:
            if (not math.isnan(ratings_prediction_test.ix[user, col])):
                movies_rated_test.append(col)

        check_prediction_dict = {}

        for movie in movies_rated_test:
            if(not movie in list(train_data.columns)):
                continue
            users_rated = train_data[train_data[movie] > 0]
            similarity_list = users_similarity_matrix.ix[user, list(users_rated.index)]
            similarity_list.sort_values(inplace=True, ascending=False)
            similarity_list = similarity_list[0:k]
            num = 0
            deno = 0
            avg = train_data.mean(axis=1)[user]
            for similar_user in list(similarity_list.index):
                user_avg = train_data.mean(axis=1)[similar_user]
                num += similarity_list[similar_user] * (train_data.ix[similar_user, movie] - user_avg)
                deno += math.fabs(similarity_list[similar_user])
            prediction = avg + (float(num / deno))
            check_prediction_dict[movie] = prediction

        print(user,check_prediction_dict)
        for key, value in check_prediction_dict.items():
            if not math.isnan(value):
                value = math.ceil(value * 100)/100
                pred.append(value)
                actual.append(ratings_prediction_test.ix[user, key])

    rmse1 = rmse(pred,actual)
    print("The root mean square is " , rmse1)

def main():
    ratings_file_path = 'ml-100k/u.data'
    # get_data_ready(ratings_file_path)
    with open('ratings_dataframe.pickle','rb') as f:
        ratings_dataframe = pickle.load(f)
    with open('users_similarity_dataframe.pickle','rb') as f:
        users_similarity_matrix = pickle.load(f)
    user = int(sys.argv[1])
    k=int(sys.argv[2])

    ratings_prediction=ratings_dataframe.loc[[user]].copy()
    movies_rated=[]
    movies_not_rated=[]

    for col in ratings_prediction.columns:
        if(not math.isnan(ratings_prediction.ix[user,col])):
            movies_rated.append(col)
        else:
            movies_not_rated.append(col)
    if(movies_rated.__len__()<20):
        ordered_recommendation=get_k_most_popular_movies()
    else:
        prediction_dict={}

        for movie in movies_not_rated:
            users_rated = ratings_dataframe[ratings_dataframe[movie] > 0]
            similarity_list = users_similarity_matrix.ix[user, list(users_rated.index)]
            similarity_list.sort_values(inplace=True, ascending=False)
            similarity_list = similarity_list[0:k]
            num = 0
            deno = 0
            avg = ratings_dataframe.mean(axis=1)[user]
            for similar_user in list(similarity_list.index):
                user_avg = ratings_dataframe.mean(axis=1)[similar_user]
                num += similarity_list[similar_user] * (ratings_dataframe.ix[similar_user, movie] - user_avg)
                deno += math.fabs(similarity_list[similar_user])
            prediction = avg + (float(num / deno))
            prediction_dict[movie] = prediction
        ordered_recommendation = (sorted(prediction_dict,key=lambda k: (prediction_dict[k], -k), reverse=True))

    print("Movies rated by the user with Id {0} are: ".format(user))
    displayUserWatchHistory(movies_rated)
    print("Top 5 recommended movies for userId ",user," are : ",ordered_recommendation[0:5])
    displayMovieInfo(ordered_recommendation[0:3])

if __name__ == '__main__':
    # main()
    accuracy_calculation()
